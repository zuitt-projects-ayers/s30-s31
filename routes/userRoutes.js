// import express
const express = require('express');

// assign Router()
const router = express.Router();

// import controllers for users
const userControllers = require('../controllers/userControllers');

// create route(s)
router.post('/', userControllers.createUserController);

router.get('/', userControllers.getAllUsersController);

router.put('/:id', userControllers.updateUserStatusController);

router.get('/getSingleUser/:id', userControllers.getSingleUserController);

// export router
module.exports = router;