// Note: All packages/modules should be required at the top of the file to avoid tampering or errors.

const express = require('express');

// Mongoose is a package that uses an ODM or object document mapper. It allows us to translate our JS objects into database documents for MongoDB. It allows connection and easier manipulation of our documents in MongoDB.
const mongoose = require('mongoose');

const taskRoutes = require('./routes/taskRoutes')

const userRoutes = require('./routes/userRoutes') // for activity 1

const port = 4000;

const app = express();

/*
	Syntax:
		mongoose.connect("<connectionStringFromMongoDBAtlus>", {
			useNewUrlParser : true,
			useUnifiedTopology : true
		})
*/
mongoose.connect("mongodb+srv://admin-ayers:admin169@ayers-169.mgg6c.mongodb.net/task169?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
});
// Create notifications if the connection to the db is successful or not.

let db = mongoose.connection;

// We add this so that when db has a connection error, we will show the connection error both in the terminal and in the browser for our client.
db.on('error', console.error.bind(console, 'Connection Error.'));

// Once the connection is open and successful, we will output a message in the terminal.
db.once('open', () => console.log('Connected to MongoDB.'))

app.use(express.json());

// Our server will use a middleware to group all task routes under /tasks .
// All the endpoints in the taskRoutes file will start with /tasks .
app.use('/tasks', taskRoutes);

app.use('/users', userRoutes) // for activity 1

app.listen(port, () => console.log(`Server is running at port ${port}.`))
