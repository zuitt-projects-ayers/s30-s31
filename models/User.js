// import mongoose
const mongoose = require('mongoose')

// create schema
const userSchema = new mongoose.Schema(
	{
		username : String,
		password : String
	}
);

// export schema
module.exports = mongoose.model("User", userSchema);