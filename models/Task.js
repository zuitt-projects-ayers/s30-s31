const mongoose = require('mongoose');

// Mongoose Schema
	// Before we can create documents form our api to save into our database, we first have to determine the structure of the documents to be written in the database.

	// A schema is a blueprint for our document.

	// Schema() is a constructor from mongoose that will allow us to create a new schema object.

const taskSchema = new mongoose.Schema(

	/*
		Define the fields for our task document.
		The task document should have name and status fields.
		Both of the fields should have a data type of string.
	*/
	
	{
		name : String,
		status : String
	}
)

// Mongoose model
	// Models are used to connect your api to the corresponding collection in your database. It is a representation of the Task document.

	// Models use schema to create objects that correspond to the schema. By default, when creating the collection from your model, the collection name is pluralized.

	// Syntax: mongoose.model(<nameOfCollectionInAtlas>, <schemaToFollow>)

module.exports = mongoose.model("Task", taskSchema);

// module.exports will allow us to export files/functions and be able to import/require them in another file within our application.

// Export the model into other files that may require it.

// (Multiple models require multiple files.)