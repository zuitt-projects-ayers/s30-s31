const Task = require('../models/Task');

module.exports.createTaskController = (req, res) =>
{
	console.log(req.body);
	Task.findOne({name : req.body.name})
	.then(result => {

		if (result !== null && result.name === req.body.name) {
			return res.send('Duplicate task found.');

		} else {
			
			let newTask = new Task({
				name : req.body.name,
				status : req.body.status
			})
			newTask.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	})
	.catch(error => res.send(error));
}

module.exports.getAllTasksController = (req, res) => {

	// just like db.tasks.find({})
	Task.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

module.exports.getSingleTaskController = (req, res) => {

	console.log(req.params);

	// let id = req.params.id
	// Task.findById(id)

	Task.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

module.exports.updateTaskStatusController = (req, res) =>
{
	console.log(req.params.id);
	console.log(req.body);

	let updates = {
		status: req.body.status
	}

	// {new: true} - allows us to return the updated version of the document we were updating. By default, without this arugment, findByIdandUpdate will return the previous state of the document or previous version.
	Task.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

// Sir Wilson
/*
promises are basically values that are unknown now, but will get a value later, and .then() and .catch() are like if-else statements to that unknown value, .then() will have code that handles the result of that unknown value, and .catch() is for handling errors involving that unknown value

a promise can be something as simple as a coin toss, we don't know the result in the end, so we write code to handle the possible values via .then(0 and .catch()
*/

// Ma'am Kim
// These controllers use MVP (model-view-controller) pattern.