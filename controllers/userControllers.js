// import user schema/model
const User = require('../models/User');

// create and export controller(s)
module.exports.createUserController = (req, res) => {

	console.log(req.body);
	User.findOne({username : req.body.username})
	.then(result => {

		if (result !== null && result.username === req.body.username) {
			return res.send('Username has been already been taken.');

		} else {

			let newUser = new User({
				username : req.body.username,
				password : req.body.password
			})
			newUser.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));

		}

	})
	.catch(error => res.send(error));

};


module.exports.getAllUsersController = (req, res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));

};


module.exports.updateUserStatusController = (req, res) => {

	// log info to check
	console.log(req.params.id);
	console.log(req.body);

	let updates = {
		username : req.body.username
	};

	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(result => res.send(result))
	.catch(error => res.send(error));
}


module.exports.getSingleUserController = (req, res) => {

	// log info to check
	console.log(req.params)

	User.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));
}